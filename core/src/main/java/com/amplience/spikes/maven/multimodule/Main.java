package com.amplience.spikes.maven.multimodule;


import com.amplience.spikes.maven.multimodule.ui.MainWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;

public class Main {
    static Logger LOG = LoggerFactory.getLogger(Main.class);
    private MainWindow ui;

    public Main() throws InvocationTargetException, InterruptedException {
        startUI();
    }

    private void startUI() throws InvocationTargetException, InterruptedException {
        SwingUtilities.invokeAndWait(new Runnable() {
            @Override
            public void run() {
                ui = new MainWindow();
            }
        });
    }

    public static void main(String... args) throws InvocationTargetException, InterruptedException {
        Main main = new Main();
    }
}
