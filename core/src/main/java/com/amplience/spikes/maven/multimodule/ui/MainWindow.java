package com.amplience.spikes.maven.multimodule.ui;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

public class MainWindow extends JFrame {
    static Logger LOG = LoggerFactory.getLogger(MainWindow.class);

    public static final String MAIN_WINDOW_NAME = "Simple Main Window";
    public static final String APPLICATION_TITLE = "Simple Multi-Module Maven Project";

    public MainWindow() {
        super(APPLICATION_TITLE);
        setName(MAIN_WINDOW_NAME);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
