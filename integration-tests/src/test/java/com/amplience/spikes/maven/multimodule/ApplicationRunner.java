package com.amplience.spikes.maven.multimodule;


import com.amplience.spikes.maven.multimodule.ui.MainWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.fail;

public class ApplicationRunner {
    static Logger LOG = LoggerFactory.getLogger(ApplicationRunner.class);

    private ApplicationDriver driver;

    public void starts() throws Exception {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Main.main();
                } catch (Exception e) {
                    LOG.error("Error starting Main", e);
                    fail("Unable to start Main");
                }
            }
        }, "End-to-End Tests");
        thread.setDaemon(true);
        thread.start();

        driver = new ApplicationDriver();
        driver.hasTitle(MainWindow.APPLICATION_TITLE);
    }
}
