package com.amplience.spikes.maven.multimodule;


import com.amplience.spikes.maven.multimodule.ui.MainWindow;
import com.objogate.wl.swing.AWTEventQueueProber;
import com.objogate.wl.swing.driver.JFrameDriver;
import com.objogate.wl.swing.gesture.GesturePerformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class ApplicationDriver extends JFrameDriver {
    static Logger LOG = LoggerFactory.getLogger(ApplicationDriver.class);

    public ApplicationDriver() {
        this(1);
    }

        @SuppressWarnings("unchecked")
    public ApplicationDriver(int timeout) {
        super(new GesturePerformer(),
                JFrameDriver.topLevelFrame(
                        named(MainWindow.MAIN_WINDOW_NAME),
                        showingOnScreen()),
                        new AWTEventQueueProber(TimeUnit.SECONDS.toMillis(timeout), 100));
    }
}
