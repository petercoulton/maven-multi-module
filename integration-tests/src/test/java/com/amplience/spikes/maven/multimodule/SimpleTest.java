package com.amplience.spikes.maven.multimodule;


import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleTest {
    static Logger LOG = LoggerFactory.getLogger(SimpleTest.class);

    public final ApplicationRunner application = new ApplicationRunner();

    @Test
    public void
    should_show_main_window() throws Exception {
        application.starts();
    }
}
